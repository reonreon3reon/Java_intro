//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-10-20 

public class Matrix {
  public static void main(String[] args) {
    double[][] matA = { 
      {1.0, 2.0}, 
      {3.0, 4.0} 
    };
    double[][] matB = { 
      {1.5, 0.0}, 
      {0.0, 1.5} 
    };
    double[][] matR = new double[2][2];
    for (int i = 0; i < matA.length; i++) {
      for (int k = 0; k < matR.length; k++) {
        for (int j = 0; j < matB.length; j++) {
          matR[i][k] += matA[i][j]*matB[j][k];
        }
      }
    }
    for (int i = 0; i < matR.length; i++) {
      for (int j = 0; j < matR.length; j++) {
        System.out.print(matR[i][j] + " ");
      }
      System.out.println("");
    }
  }
}
