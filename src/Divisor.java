/* 二つの整数値A、Bを読み込んで，BがAの約数かどうかを判定するプログラム(クラス名はDivisor)を作りなさい */

import java.io.*;

public class Divisor{
  public static void main(String[] args) {
    BufferedReader reader1 = new BufferedReader(new InputStreamReader(System.in));
    BufferedReader reader2 = new BufferedReader(new InputStreamReader(System.in));
    try {
      System.out.println("A?");
      String line1 = reader1.readLine();
      System.out.println("B?");
      String line2 = reader2.readLine();

      final double i1 = Integer.parseInt(line1);
      final double i2 = Integer.parseInt(line2);
      final double i3  = i1/i2;
      if(i3 - Math.rint(i3) == 0){
        System.out.println((int)i1 + "は" + (int)i2 +"の約数です. " );
      }else{
        System.out.println((int)i1 + "は" + (int)i2 +"の約数ではありません. " );
      }
    }catch (IOException e) {
      System.out.println(e);
    }   
  }
}
