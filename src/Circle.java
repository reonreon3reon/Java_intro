//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2015-12-01

public class Circle extends GeometricObject {
  private double radius;

  public Circle() {
  }

  public Circle(double radius) {
    this.radius = radius;
  }

  public Circle(double radius, String color, boolean filled) {
    this.radius = radius;
    setColor(color);
    setFilled(filled);
  }

  public double getRadius() {
    return radius;
  }

  public void setRadius() {
    this.radius = radius;
  }

  public double getArea() {
    // Write your code here
    return radius * radius * Math.PI;
  }

  public double getDiameter() {
    return 2 * radius;
  }

  public double getPerimeter() {
    // Write your code here
    return getDiameter()* Math.PI;
  }
}
