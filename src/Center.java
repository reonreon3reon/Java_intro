//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-07-28 

import java.io.*;

public class Center {
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      String line;
      String space80 = "                                                                                ";
      while ((line = reader.readLine()) != null) {
        String s = space80.substring(line.length()/2) + line;
        System.out.println(s);

        /* String s = space40.substring(line.length()) + line; */
        /* System.out.println(s); */
      }
    } catch (IOException e) {
      System.out.println(e);
    }
  }
}
