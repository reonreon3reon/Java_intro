/* 上の例題のプログラムを参考にして，for文を使って50から100の偶数を足しあわせ， (つまり， */
/*   50 + 52 + … + 100 */
/* を計算し)，合計を出力するクラスEvenSumを作成しなさい．  */

import java.io.*;

public class Print2 {
  public static void main(String[] args) {
    String line = "ABC";
    for (int i = 1; i <= 10; i++) {
      System.out.print(line.charAt(i % 3));
      if (i % 4 == 0) {
        System.out.println("");
      }
    }
    System.out.println("");
  }
}
