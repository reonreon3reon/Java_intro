//今月(2015年7月)の日付(1〜31の整数) を入力すると、その曜日をプリントする プログラムを作りなさい(クラス名はDay)。 日付が1〜31の範囲に収まっているかどうかの判定もすること。 

/* July 2015 */
/* Su Mo Tu We Th Fr Sa */
         /* 1  2  3  4 */
/* 5  6  7  8  9 10 11 */
/* 12 13 14 15 16 17 18 */
/* 19 20 21 22 23 24 25 */
/* 26 27 28 29 30 31 */


/* 12日は日曜日です。 */

import java.io.*;

public class Day{
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      System.out.println("日付を入力してください。");
      String line = reader.readLine();
      int n = Integer.parseInt(line);
      if( (n <= 0) || (31 < n) ){
        System.out.println("日付が正しくありません。" );
        System.exit(0);
      }
      switch (n%7) {
        case 5:
          System.out.println(n + "日は日曜日です。" );
          break;
        case 6:
          System.out.println(n + "日は月曜日です。" );
          break;
        case 0:
          System.out.println(n + "日は火曜日です。" );
          break;
        case 1:
          System.out.println(n + "日は水曜日です。" );
          break;
        case 2:
          System.out.println(n + "日は木曜日です。" );
          break;
        case 3:
          System.out.println(n + "日は金曜日です。" );
          break;
        case 4:
          System.out.println(n + "日は土曜日です。" );
          break;
        default:
          System.out.println("日付が正しくありません。");
          break;
      }
    } catch (IOException e) {
      System.out.println(e);
    } catch (NumberFormatException e) {
      System.out.println("数字の形式が正しくありません。");
    }
  }
}
