//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-10-06 


import java.io.*;

public class  CharInBoth{
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      String line = reader.readLine();
      String line2 = reader.readLine();
      int foundLine = 0;
      boolean found = false;
      for(int i = 0; i < line.length(); i++){
        for(int j = 0; j < line2.length(); j++){
          if(line.charAt(i) == line2.charAt(j)){
            foundLine = i;
            found = true;
            break;
          }
        }
      }
      if(found){
        System.out.println("同じ文字'" + line.charAt(foundLine) + "'が見つかりました．");
      }else{
        System.out.println("同じ文字が見つかりませんでした．");
      }
    }catch (IOException e) {
    }
  }
}
