//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-07-28 

/* 任意の正の整数について， */
/* 『偶数なら2で割る．奇数なら3倍して1を足す．』 */
/*   という操作を繰り返すと，やがて必ず1になる． */
/* (角谷・コラッツの予想) */


import java.io.*;

public class Kakutani {
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    System.out.println("整数を入力してください．");
    try {
      String line = reader.readLine();
      int n = Integer.parseInt(line),i = 0;
      while(n != 1){
        if(n %2 == 0){
          n = n/2;
        }else{
          n = 3*n+1;
        }
        i++;
        System.out.println(i + "回:" + n);
      }
      System.out.println(i + "回で1になりました．");
    }
    catch (IOException e) {
      System.out.println(e);
    } catch (NumberFormatException e) {
      System.out.println("数字の形式が正しくありません。");
    }
  }
}
