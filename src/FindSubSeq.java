//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-10-27 

/* borage02:~ kawasima$ java FindSubSeq 3 2 1 */
/* Input 10 data: 5 */
/* 3 */
/* 2 */
/* 1 */
/* 8 */
/* 3 */
/* 2 */
/* 3 */
/* 2 */
/* 1 */
/* Found at 1 Sequence: 3 2 1  */
/* Found at 7 Sequence: 3 2 1  */

import java.io.*;

public class FindSubSeq {
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      String line;
      System.out.print("Input 10 data: ");
      int a[] = new int[10];
      boolean n = false;
      for( int i = 0; i < 10; i++ ){
        line = reader.readLine();
        a[i] = Integer.parseInt(line);
      }
      for( int i = 0; i < 10 - args.length+1; i++ ){
        n = false;
        for( int j = 0; j < args.length; j++ ){
						n = (a[i+j] == Integer.parseInt(args[j])) ? true : false;
        }
        if(n){
          System.out.print("Found at "+ i + " Sequence: ");
          int k = 0;
          for(; k < args.length - 1; k++ ){
            System.out.print(args[k] + " ");
          } System.out.println(args[k]);
        }
      }
    } catch(IOException e){
      System.out.println(e);
    } catch(NumberFormatException e){
      System.out.println("数字の形式が正しくありません．");
    }
  }
}
