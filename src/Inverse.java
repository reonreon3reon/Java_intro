//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-10-20 

import java.io.*;

public class Inverse
{
  public static void reverse(int myArray[]){
    System.out.println("Result:");
    for (int i = 0; i < myArray.length; i++) {
      System.out.println(myArray[(myArray.length-1) - i]);
    } 
  }

  public static void main(String[] args)
  {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    int[] myArray;

    try {
      myArray = new int[10];
      System.out.println("Input 10 values");
      for (int i = 0; i < myArray.length; i++) {
        String line = br.readLine();
        myArray[i] = Integer.parseInt(line);
      }
      reverse(myArray);
    }
    catch (IOException e){
    }
  }
}   
