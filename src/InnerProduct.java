//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-10-20 


import java.io.*;

public class InnerProduct
{
  public static void main(String[] args)
  {
    double myArrayA[] = {1.0, 1.5, 2.0, 2.5};
    double myArrayB[] = {3.1, 2.1, 1.1, 0.1};
    double result = 0.0;

    for (int i = 0; i < myArrayA.length; i++) {
      result += myArrayA[i] * myArrayB[i];
    }
    System.out.println("内積は" + result + "です。");
  }
}
