//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2016-01-12

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;

class ListString {
  static int numOfElements = 0;
  int id;
  String name;
  ListString next;

  // Constructor
  ListString(String name, ListString tail) {
    this.id = numOfElements;
    this.name = new String(name);
    this.next = tail;

    numOfElements++;
  }

  static ListString Insert(String s, ListString tail) {
    return new ListString(s, tail);
  }

  static boolean isZero() {
    return numOfElements == 0;
  }

  static void Display(ListString element) {
    while (element != null) {
      System.out.print(element.name+"[" + element.id + "]"+"-->");
      element = element.next;
    }
    System.out.println("null");
  }

  // count element
  static int length() {
    return numOfElements;
  }

  // Q2
  static void DisplayReverse(ListString element) {
    if (element == null) {
      System.out.print("null");
    } else {
      DisplayReverse(element.next);
      System.out.print("<--" + element.name+"[" + element.id + "]");
    }
  }
  // Q3
  static void Search(String query, ListString head) {
    if (head == null) {
      System.out.println("[Search] not found " + query);
    } else if (head.name.equals(query)) {
      System.out.println("[Search] found: " + head.name+"[" + head.id + "]");
    } else {
      Search(query, head.next);
    }
  }
  // FileRead
  static ListString FileRead() {
    ListString head = null;
    try {
      File file = new File("strings.txt");
      BufferedReader br = new BufferedReader(new FileReader(file));
      while (true) {
        String str = br.readLine();
        if (str == null) break;
        if (ListString.isZero()) {
          head = new ListString(str, null);
        } else {
          head = new ListString(str, head);
        }
      }
      br.close();
    } catch(FileNotFoundException e){
      System.out.println(e);
    } catch(IOException e){
      System.out.println(e);
    }
    return head;
  }
  static ListString _cutTail(ListString element, int k) {
    // length() - k is a habit func to BubbleSort.
    for(int i = 0; i < element.length() - k ; i++) {
      element = element.next;
    }
    return element;
  }

  // BubbleSort
  public static ListString BubbleSort(ListString element) {
    boolean needNextPass = true;
    ListString head = element;
    for (int k = 1; k < ListString.length() && needNextPass; k++) {
      needNextPass = false;
      element = head;
      // -k was implemented in following _cutTail().
      for (int i = 0; element.next !=
          _cutTail(head, k).next; ++i) {
        if (element.name.compareTo(element.next.name) < 0) {
          int tmp_id = element.id;
          String tmp_name = element.name;
          element.id = element.next.id;
          element.name = element.next.name;
          element.next.id = tmp_id;
          element.next.name = tmp_name;
        } else {
        }
        element = element.next;
        needNextPass = true;
          }
    }
    return head;
  }
}

// ListBubble.java
class ListBubble
{
  public static void main (String[] args)
  {
    ListString head;

    System.out.println("Before sorting");
    head = ListString.FileRead();
    ListString.Display(head);

    System.out.println("After sorting");
    head = ListString.BubbleSort(head);
    ListString.Display(head);
  }
}
