//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-10-13 


import java.io.*;

public class Binary {
  public static void printBinary(int n) {
    if(n/2 != 0){ 
      printBinary(n/2);
    }
    System.out.print(n%2);
  }
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      String line;
      line = reader.readLine();
      int a = Integer.parseInt(line);
      printBinary(a);
      System.out.println("");
    } catch(IOException e){
      System.out.println(e);
    } catch(NumberFormatException e){
      System.out.println("数字の形式が正しくありません．");
    }
  }
}
