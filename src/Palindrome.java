//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2015-10-06


import java.io.*;

public class Palindrome{
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      String line = reader.readLine();
      int len;
      boolean flag=true;
      for(int i=0; i < line.length(); i++){
        len = (line.length() - 1) - i;
        if(line.charAt(i) != line.charAt(len)){
          flag = false;
        }
      }
      if(flag){
        System.out.println(line + "は回文です。");
      }else{
        System.out.println(line + "は回文ではありません。");
      }
    } catch (IOException e) {
    }
  }
}
