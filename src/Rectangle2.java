//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-11-17 


import java.io.*;

public class Rectangle2 {
  int width;
  int height;

  Rectangle2() {
    setSize(10, 20);
  }

  @Override
  public String toString() {
    return "[ width = " + width + ", height = " + height + " ]";
  }

  void setSize(int w, int h) {
    width = w > 0 ? w : 0;
    height= h > 0 ? h : 0;
  }

  public static void main(String[] args) {
    Rectangle2 r = new Rectangle2();
    r.setSize(1, 2);
    System.out.println(r);
    r.setSize(-1, -2);
    System.out.println(r);
  }
}
