//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-11-17 


import java.io.*;

public class Student {
  String name;
  int[] score;
  public Student(String name, int x, int y, int z) {
    this.name = name;
    int[] scores=new int[3];
    scores[0] = x;
    scores[1] = y;
    scores[2] = z;
    score = scores;
  }
  public String toString() {
    String s = "[" + name;
    for (int i = 0; i < score.length; i++) {
      s += "," + score[i];
    }
    s += "]";

    return s;
  }
  public int total() {
    int sum = 0;
    for (int i = 0; i < score.length; i++) {
      sum += score[i];
    }
    return sum;
  }
}
