/* 上の例題のプログラムを参考にして，for文を使って50から100の偶数を足しあわせ， (つまり， */
/*   50 + 52 + … + 100 */
/* を計算し)，合計を出力するクラスEvenSumを作成しなさい．  */

import java.io.*;

public class EvenSum{
  public static void main(String[] args) {
    int sum = 0;
    for (int i = 50; i <= 100; i+=2) {
      sum += i;
    }
    System.out.println(sum);
  }   
}
