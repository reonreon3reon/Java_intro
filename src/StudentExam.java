//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-11-17 


import java.io.*;

public class StudentExam {
  public static void main(String[] args) {
    Student[] data = {
      new Student("Alice" , 70, 80, 90),
      new Student("Bob"   , 60, 50, 70),
      new Student("Taro"  , 80, 60, 90),
      new Student("Hanako", 90, 40, 80),
    };
    for (int i = 0; i < data.length; i++) {
      System.out.println(data[i] + " -> " + data[i].total());
    }
  }
}
