//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-11-17 


import java.io.*;

public class Rectangle {
  int width;
  int height;

  Rectangle() {
    setSize(10, 20);
  }

  void setSize(int w, int h) {
    width = w;
    height = h;
  }

  int getArea() {
    return width * height;
  }

  boolean areSame(Rectangle b) {
    if (this == null || b == null) {
      return false;
    }
    else if (this.width == b.width && this.height == b.height) {
      return true;
    }
    else {
      return false;
    }
  }

  public static void main(String[] args) {
    Rectangle r1 = new Rectangle();
    Rectangle r2 = new Rectangle();
    System.out.println(r1.areSame(r1));
  }
}

