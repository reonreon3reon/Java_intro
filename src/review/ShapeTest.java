import java.io.*;

abstract class Shape {
  abstract double area();
  abstract double circumference();
}

class Circle extends Shape {
  static final double PI = 3.14;
  double r;

  Circle(double r) { this.r = r; }

  double getRadius() { return r; }
  @Override
  double area() { return PI*r*r; }
  @Override
  double circumference() { return 2*PI*r; }
}

class Rectangle extends Shape {
  double w, h;

  Rectangle(double w, double h) {
    this.w = w;
    this.h = h;
  }

  double getWidth() { return w; }
  double getHeight() { return h; }
  @Override
  double area() { return w*h; }
  @Override
  double circumference() { return 2*(w+h); }
}

interface Centered {
  void setCenter(double x, double y);
  double getCenterX();
  double getCenterY();
}

class CenteredRectangle extends Rectangle implements Centered {
  double cx, cy;

  CenteredRectangle(double cx, double cy, double w, double h) {
    super(w, h);
    this.cx = cx;
    this.cy = cy;
  }

  @Override
  public void setCenter(double x, double y) { cx = x; cy = y; }
  @Override
  public double getCenterX() { return cx; }
  @Override
  public double getCenterY() { return cy; }
}

class CenteredCircle  extends Circle implements Centered {
  double cx, cy;

  CenteredCircle(double cx, double cy, double r) {
    super(r);
    this.cx = cx;
    this.cy = cy;
  }
  @Override
  public void setCenter(double x, double y) { cx = x; cy = y; }
  @Override
  public double getCenterX() { return cx; }
  @Override
  public double getCenterY() { return cy; }
}

class CenteredSquare extends Rectangle implements Centered {
  double cx, cy;

  CenteredSquare(double cx, double cy, double w) {
    super(w, w);
    this.cx = cx;
    this.cy = cy;
  }

  @Override
  public void setCenter(double x, double y) { cx = x; cy = y;}
  @Override
  public double getCenterX() { return cx; }
  @Override
  public double getCenterY() { return cy; }
}

class ShapeTest {
  public static void main(String[] args) {
    Shape [] shape = new Shape[3];
    shape[0] = new Circle(2.0);
    shape[1] = new Rectangle(1.0, 3.0);
    shape[2] = new Rectangle(4.0, 2.0);
    double totalArea = 0.0;

    for (int i = 0; i < shape.length; i++ )
      totalArea += shape[i].area();

    System.out.println("" + Math.round(totalArea));

    Shape [] shape2 = new Shape[3];
    shape2[0] = new CenteredCircle(1.0, 1.0, 1);
    shape2[1] = new CenteredSquare(2.0, 2.0, 3);
    shape2[2] = new CenteredRectangle(3.0, 3.0, 3, 4);

    double totalArea2 = 0.0;
    double totalDistance2 = 0.0;
    for (int i = 0; i < shape2.length; i++ ) {
      totalArea2 += shape2[i].area();
      Centered c = (Centered) shape2[i];
      double cx = c.getCenterX();
      double cy = c.getCenterY();
      totalDistance2 += Math.sqrt(cx*cx* + cy*cy);
    }
    System.out.println(Math.round(totalArea2/shape2.length));
    System.out.println(Math.round(totalDistance2/shape2.length));
  }
}
