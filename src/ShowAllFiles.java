//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2015-12-15

import java.io.*;

public class ShowAllFiles {
  public static void main(String[] args) throws IOException {
    if (args.length != 1) {
      System.out.println("使用法：java ShowAllFiles ディレクトリ名");
      System.out.println("例：java ShowAllFiles doc");
      System.exit(0);
    }
    String dirname = args[0];
    File dir = new File(dirname);
    String[] filenames = dir.list();
    for(int i = 0; i < filenames.length; i++) {
      String filename = dirname + "/" + filenames[i];
      try {
        System.out.println("File: " + filename);
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        String line;
        while ((line = reader.readLine()) != null) {
          System.out.println(line);
        }
        reader.close();
        System.out.println("");
      } catch (FileNotFoundException e) {
        System.out.println(filename + "が見つかりません。");
      } catch (IOException e) {
        System.out.println(e);
      }
    }
  }
}

