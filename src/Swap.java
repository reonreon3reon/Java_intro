public class Swap {
  public static void main(String[] args) {
    int x = 3;
    int y = 5;
    /* ここに適切なプログラム数行を入れる。 */
    int tmp = x;
    x = y;
    y = tmp;
    System.out.println("x : " + x);
    System.out.println("y : " + y);
  }
}
