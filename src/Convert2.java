//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-07-28 


import java.io.*;

public class Convert2 {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                String s = line.replace("。", "+");
                s = s.replace("、", "-");
                s = s.replace(".", "*");
                s = s.replace(",", "/");

                s = s.replace("*", "。");
                s = s.replace("/", "、");
                s = s.replace("+", ".");
                s = s.replace("-", ",");
                System.out.println(s);
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
