import java.io.*;

public class Count_a {
  public static void main(String[] args) {
    System.out.println("文字列を入力してください。");
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      String line = reader.readLine();
      int a=0;
      for (int i = 0; i < line.length(); i++) {
        if(line.charAt(i) == 'a'){
          a++;
        }
      }
      System.out.println("'a'の数は" + a + "個です。");
    } catch (IOException e) {
      System.out.println(e);
    }
  }
}
