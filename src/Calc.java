class Calc {

    public static void main(String[] args) {

        int i = 9;

        System.out.println("A. " + i + 5);
        System.out.println("B. " + (i * 5));
        System.out.println("C. " + (i / 5));
        System.out.println("D. " + (i % 5);
        System.out.println("E. " + (i - 5));
    }
}
