/* 上の例題のプログラムを参考にし， 正の整数 n をキーボードから入力すると， 1からnまでの整数の二乗の和 */
/* 12 + 22 + … +n 2 */
/* を，for文を使って計算し，出力するクラスSquareSumを作成しなさい．  */

import java.io.*;

public class SquareSum {
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      System.out.println("正の整数を入力してください。");
      String line = reader.readLine();
      int n = Integer.parseInt(line);
      int sum = 0;
      for (int i = 1; i <= n; i++) {
        sum += i*i;
      }
      System.out.println("1から" + n + "までの二乗和は" + sum);
    } catch (IOException e) {
      System.out.println(e);
    } catch (NumberFormatException e) {
      System.out.println("数字の形式が正しくありません。");
    }
  }
}

