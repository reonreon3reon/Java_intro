//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2016-02-08
// ComputeFibonacci.java
import java.util.Scanner;

public class ComputeFibonacci {
  static int count = 0;
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    System.out.print("Enter an index for a Fibonacci number: ");
    int index = input.nextInt();
    System.out.println("The Fibonacci number at index " + index + " is " + fib(index));
    System.out.println("The number of invocations at index " + index + " is " + count);
  }

  public static long fib(long index) {
    if (index == 0) {
      count += 1;
      return 0;
    }
    else if (index == 1) {
      count += 1;
      return 1;
    }
    else {
      count += 1;
      return fib(index-1) + fib(index-2);
    }
  }
}
