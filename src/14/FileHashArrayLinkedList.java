// FileHashArrayLinkedList.java
class FileHashArrayLinkedList {
  static int SzBkt;

  static void HashSearch(String s, ListString HashTable[])
  {
    ListString.Search(s, HashTable[HashFunction(String2Integer(s))]);
  }

  static int String2Integer(String s) {
    int result = 0;

    result = (int)s.charAt(0);

    return result;
  }

  // Note that SzBkt is a static variable
  static int HashFunction(int l) {
    return l % SzBkt;
  }

  public static void main (String[] args) {
    System.out.println("SzBkt,Time");
    for( int q = 1; q < 100; q++ ){
      SzBkt = q;
      ListString [] HashTable = new ListString[SzBkt];
      ListString data;
      ListString head;

      for (int i = 0; i < SzBkt; i++) {
        HashTable[i] = null;
      }

      // Constructing a hash structure
      head = ListString.FileRead();
      for (data = head; data != null; data = data.next) {
        int s2int = String2Integer(data.name);
        int pos = HashFunction(s2int);
        HashTable[pos] = ListString.Insert(data.name, HashTable[pos]);
      }

      // Search
      long start, end;

      start = System.currentTimeMillis();
      for (data = head; data != null; data = data.next) {
        HashSearch(data.name, HashTable);
      }
      end = System.currentTimeMillis();
      System.out.println(q + "," + (end - start));
    }
  }
}
