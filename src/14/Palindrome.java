//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2016-02-02

public class Palindrome {
  public static boolean isPalindrome(String s) {
    if (s.length() <= 1)
      // Please write your code here
      return true;
    else if (s.charAt(0) != s.charAt(s.length() - 1))
      // Please write your code here
      return false;
    else
      return isPalindrome(s.substring(1, s.length() - 1));
  }

  public static void main(String[] args) {
    ListString head = ListString.FileRead();
    while (head != null) {
      if (isPalindrome(head.name)) {
        System.out.println(head.name);
      }
      head = head.next;
    }
  }
}
