//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2015-11-24


import java.io.*;

class Car {
  private int id;
  private double gas;

  void set(int id, double gas) {
    this.id = id;
    this.gas = gas;
  }

  void show() {
    System.out.println("id: " + this.id);
    System.out.println("gas: " + this.gas);
  }
}

class Quiz1 {
  public static void main(String[] args) {
    Car car1 = new Car();
    car1.set(1, 10.0);
    car1.show();
  }
}
