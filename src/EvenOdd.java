/* 整数を入力してください。 */
/* 3 */
/* 3は奇数です。 */

import java.io.*;

public class EvenOdd {
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      System.out.println("整数を入力してください。");
      String line = reader.readLine();
      int n = Integer.parseInt(line);
      if (n % 2 == 1) {
        System.out.println(n + "は奇数です。");
      } else {
        System.out.println(n + "は偶数です。");
      }
    } catch (IOException e) {
      System.out.println(e);
    } catch (NumberFormatException e) {
      System.out.println("数字の形式が正しくありません。");
    }
  }
}
