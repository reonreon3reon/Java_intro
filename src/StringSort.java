//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-11-9 

import java.io.*;

public class StringSort {
	public static void main(String[] args) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try {
			String line;
			int linenum = 0;
			String lines[] = new String[1000];
			while ((line = reader.readLine()) != null) {
				lines[linenum++] = line;
			}
			for (int i = linenum-1; i >= 0; --i) {
				for(int j = i; j < linenum-1; ++j) {
					if(lines[j].compareTo(lines[j+1]) > 0) {
						String s = new String( lines[j] );
						lines[j] = new String( lines[j+1]);
						lines[j+1] = s;
					}
				}
			}
			for(int i = 0; i < linenum; ++i) {
				System.out.println(lines[i]);
			}
		} catch (IOException e) {
			System.out.println(e);
		}
	}
}
