//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-10-06 


import java.io.*;

/*
  def is_prime(n): 
  i = 2
  while i * i <=n:
    if n % i == 0:
      return False
    i += 1
  return True 
   */

public class Prime{
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try{
      String line = reader.readLine();
      int n = new Integer(line).intValue();
      int i = 2;
      boolean flag = true;
      while( i * i <= n ){
        if( n%i == 0){
          flag = false;
        }
        i += 1;
      }
      if(flag){
        System.out.println(n + "は素数です。");
      }else{
        System.out.println(n + "は素数ではありません。");
      }
    }catch (IOException e) {
    }
  }
}
