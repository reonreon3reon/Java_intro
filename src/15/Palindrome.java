//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2016-02-12
import java.io.*;

public class Palindrome {

  public static boolean isPalidrome(String l) {
    if (l.length() == 1) {
      return true;
    } else if (l.length() <= 0) {
      return false;
    } else {
      for(int i = 0; i < l.length(); i++) {
        if( l.charAt(i) != l.charAt(l.length() - 1 - i) )
          return false;
      }
      return true;
    }
  }

  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    String line;
    try {
      System.out.print("Enter a string: ");
      line = reader.readLine();
      if (isPalidrome(line)) {
        System.out.println(line + " is a palidrome");
      } else {
        System.out.println(line + " is not a palidrome");
      }
    } catch (IOException e) {
    }
  }
}
