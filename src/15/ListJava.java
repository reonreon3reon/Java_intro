//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2016-02-12
// List.java
class ListString
{
  static int amount = 0;
  int id;
  String name;
  ListString next;

  // Constructor
  ListString(String name, ListString tail) {
    this.id = amount;
    this.name = new String(name);
    this.next = tail;

    amount++;
  }

  static ListString Insert(String s, ListString tail) {
    return tail;
  }

  static void Display(ListString element) {
    for(; element != null; element = element.next)
    {
      System.out.print(element.name + "[" + element.id  + "]-->");
    }
      System.out.println("null");
  }

  static int length(ListString element) {
    return amount;
  }

  static void DisplayReverse(ListString element)
  {
    if (element == null) {
        System.out.print("null");
    } else {
        DisplayReverse(element.next);
        System.out.print("<--" + element.name + "[" + element.id + "]");
    }
  }

  static void Search(String query, ListString head)
  {
    if ( head == null ) {
      System.out.println("[Search] not found: " + query);
    } else if( query.equals(head.name) ) {
      System.out.println("[Search] found: " + head.name + "[" + head.id + "]");
    } else {
      Search(query, head.next);
    }
  }

  static ListString Delete(String s, ListString head)
  {
    if ( head == null ) {
      System.out.println("[Delete] " + s+ "\nNot found: " + s);
      return head;
    } else if( s.equals(head.name) ) {
      --amount;
      System.out.println("[Delete] " + s);
      return head.next;
    } else {
      head.next = Delete(s, head.next);
      return head;
    }
  }

  static ListString DeleteDisplay(String string, ListString head)
  {
    head = ListString.Delete(string, head);
    ListString.Display(head);
    System.out.println("#Elements: " + ListString.length(head));
    return head;
  }
}

class ListJava
{
  public static void main (String[] args)
  {
    ListString head;

    head = new ListString("Tokyo", null);
    head = new ListString("Kyoto", head);
    head = new ListString("Tsukuba", head);
    head = new ListString("Nara", head);
    head = new ListString("Ueno", head);
    head = new ListString("Ehime", head);

    ListString.Display(head);
    //
    System.out.println("#Elements: " + ListString.length(head));
    //
    ListString.DisplayReverse(head);
    System.out.println("");
    //
    ListString.Search("Kyoto", head);
    ListString.Search("Fukuoka", head);
    //
    head = ListString.DeleteDisplay("Ehime", head);
    head = ListString.DeleteDisplay("Fukuoka", head);
    head = ListString.DeleteDisplay("Tokyo", head);
    head = ListString.DeleteDisplay("Tsukuba", head);
    head = ListString.DeleteDisplay("Kyoto", head);
    head = ListString.DeleteDisplay("Nara", head);
    head = ListString.DeleteDisplay("Ueno", head);
    head = ListString.DeleteDisplay("Ueno", head);
  }
}
