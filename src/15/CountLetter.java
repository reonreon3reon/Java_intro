//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2016-02-12

import java.util.Scanner;

public class CountLetter {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    System.out.print("Enter a string: ");
    String s = input.nextLine();
    int[] counts = countLetters(s.toLowerCase());
    for(int i = 0; i < 'z'-'a'; i++) {
      if (!(counts[i]== 0))
        System.out.println((char)(i+'a') + " appears " + counts[i] + " time");
    }
  }

  public static int[] countLetters(String s) {
    int[] count = new int[26];
    for(int i = 0; i < s.length(); i++) {
      char q = (char)s.charAt(i);
      if (q == ' ')
        continue;
      count[q - 'a']++;
    }
    return count;
  }
}
