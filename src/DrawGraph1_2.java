/* 上の例題のプログラムを参考にして，for文を使って50から100の偶数を足しあわせ， (つまり， */
/*   50 + 52 + … + 100 */
/* を計算し)，合計を出力するクラスEvenSumを作成しなさい．  */

import java.io.*;

public class DrawGraph1_2 {
  public static void main(String[] args) {
    int w = 10;
    for(int i = 0;i < 10; i++){
      System.out.print(i + ":");
      for(int q = 0;q < 10-w;q++){
        System.out.print(" ");
      }
      for(; w > 0;w--){
        System.out.print("*");
      }
      w = 9 - i;
      System.out.println("");
    }
  }   
}
