//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-10-06 

import java.io.*;

public class DoWhile {
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      String line;
      do {
        line = reader.readLine();
        System.out.println("You said: " + line);
      } while (line.equals("bye") == false);
    } catch (IOException e) {
    }
  }
}
