//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-10-13 


import java.io.*;

public class Combination {
  public static int comb(int m, int n) {
    if(( m == n )|| (n == 0)){
      return 1;
    }else if(n == 1){
      return m;
    }
    return comb(m-1, n-1) + comb(m-1, n);
  }
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      String line;
      line = reader.readLine();
      int a = Integer.parseInt(line);
      line = reader.readLine();
      int b = Integer.parseInt(line);
      System.out.println("comb(" + a + "," + b + ") = "+ comb(a, b));
    } catch (IOException e) {
      System.out.println(e);
    } catch (NumberFormatException e) {
      System.out.println("数字の形式が正しくありません．");
    }
  }
}
