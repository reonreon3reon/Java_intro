//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-10-13 

import java.io.*;

public class Fib {
  public static int fib(int x, int y, int n) {
    if(n > 0){
      return fib(y, x+y, n-1);
    }else{
      return x;
    }
  }
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      String line;
      line = reader.readLine();
      int a = Integer.parseInt(line);
      System.out.println(a + "番目のフィボナッチ数は" + fib(0, 1, a) + "です．");
    } catch (IOException e) {
      System.out.println(e);
    } catch (NumberFormatException e) {
      System.out.println("数字の形式が正しくありません．");
    }
  }
}
