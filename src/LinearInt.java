//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-10-27 

/* Input 10 data: 5 */
/* 4 */
/* 3 */
/* 4 */
/* 2 */
/* 7 */
/* 7 */
/* 6 */
/* 0 */
/* 4 */
/* Input key: 7 */
/* Found[5]: 7 */
/* Found[6]: 7 */
/*  */
import java.io.*;

public class LinearInt {
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      String line;
      System.out.print("Input 10 data: ");
      int a[] = new int[10];
      for( int i = 0; i < 10; i++ ){
        line = reader.readLine();
        a[i] = Integer.parseInt(line);
      }
      System.out.print("Input key: ");
      int found = Integer.parseInt(reader.readLine());
      for( int i = 0; i < 10; i++ ){
        if( a[i] == found ){ 
          System.out.println("Found["+ i + "]: " + found);
        }
      }
    } catch(IOException e){
      System.out.println(e);
    } catch(NumberFormatException e){
      System.out.println("数字の形式が正しくありません．");
    }
  }
}
