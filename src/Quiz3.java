//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2015-11-24


import java.io.*;

class Car {
  private int id;
  private double gas;

  Car() {
    super();
    this.id = 0;
    this.gas = 0.0;
  }

  Car(int id, double gas) {
    super();
    this.id = id;
    this.gas = gas;
  }

  void show() {
    System.out.println("id: " + id);
    System.out.println("gas: " + gas);
  }
}

class Quiz3 {
  public static void main(String[] args) {
    Car car1 = new Car();
    car1.show();
    Car car2 = new Car(1, 10.0);
    car2.show();
  }
}
