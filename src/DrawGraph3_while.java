//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-07-28 


import java.io.*;

public class DrawGraph3_while {
  public static void main(String[] args) {
    int i = 0, j = 0;
    while(i < 10){
      j = 0;
      System.out.print(i + ":");
      while(j < i*i){
        System.out.print("*");
        j++;
      }
      System.out.println("");
      i++;
    }
  }
}

/* public class DrawGraph3_while { */
/*   public static void main(String[] args) { */
/*     for (int i = 0; i < 10; i++) { */
/*       System.out.print(i + ":"); */
/*       for (int j = 0; j < i * i; j++) { */
/*         System.out.print("*"); */
/*       } */
/*       System.out.println(""); */
/*     } */
/*   } */
/* } */
