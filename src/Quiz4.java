//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2015-11-24


import java.io.*;

class Car {
  private static int sumCar = 0;
  private int num;
  private double gas;

  Car() {
    sumCar++;
  }

  public static void showSumCar()
  {
    System.out.println("SumCar: " + sumCar);
  }
}

class Quiz4 {
  public static void main(String[] args) {
    int max = 100;
    Car[] cars = new Car[max];
    for (int i = 0; i < max; i++) {
      cars[i] = new Car();
    }
    Car.showSumCar();
  }
}
