//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-10-13 

public class Parabola {
  public static void main(String[] args) {
    for(int i = 0; i < 17; i++){
      printGraph(sq(i-8));
    }
  }
  public static void printGraph(int x) {
    for (int i = 0; i < x; i++) {
      System.out.print("*");
    }
    System.out.println("");
  }

  public static int sq(int n) {
    return n*n;
  }
}
