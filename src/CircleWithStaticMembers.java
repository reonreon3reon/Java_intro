//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2015-11-30

import java.io.*;

public class CircleWithStaticMembers {
  double radius;
  static int numberOfObjects = 0;

  CircleWithStaticMembers() {
    // Write your code here.
    radius = 1.0;
    numberOfObjects = 1;
  }

  CircleWithStaticMembers(double newRadius) {
    // Write your code here.
    radius = newRadius;
    numberOfObjects = 2;
  }

  static int getNumberOfObjects() {
    // Write your code here.
    return numberOfObjects;
  }

  double getArea() {
    return radius * radius * Math.PI;
  }
}
