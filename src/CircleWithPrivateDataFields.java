//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2015-12-01

public class CircleWithPrivateDataFields {
  private double radius = 1;
  private static int numberOfObjects = 0;

  public CircleWithPrivateDataFields() {
    numberOfObjects++;
  }

  public CircleWithPrivateDataFields(double newRadius) {
    radius = newRadius;
    numberOfObjects++;
  }

  public double getRadius() {
    return radius;
  }

  public void setRadius(double newRadius) {
    radius = (newRadius >= 0) ? newRadius : 0;
  }

  public static int getNumberOfObjects() {
    return numberOfObjects;
  }

  public double getArea() {
    return radius * radius * Math.PI;
  }
}
