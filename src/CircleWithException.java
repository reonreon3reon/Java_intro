//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2015-12-22

public class CircleWithException {
  private double radius;
  private static int numberOfObjects = 0;
  public CircleWithException() {
    this(1.0);
  }

  public CircleWithException(double newRadius) {
    setRadius(newRadius);
    numberOfObjects++;
  }

  public double getRadius() {
    return radius;
  }

  public void setRadius(double newRadius) throws IllegalArgumentException {
    if (newRadius >= 0)
      radius = newRadius;
    else
      throw new IllegalArgumentException("Radius should be positive");
  }

  public static int getNumberOfObjects() {
    return numberOfObjects;
  }

  public double findArea() {
    return radius * radius * 3.14159;
  }
}
