import java.io.*;
public class Sum {
  public static void main(String[] args) {
    BufferedReader reader1 = new BufferedReader(new InputStreamReader(System.in));
    BufferedReader reader2 = new BufferedReader(new InputStreamReader(System.in));
    try {
      String line1 = reader1.readLine();
      String line2 = reader2.readLine();
      int i1 = Integer.parseInt(line1);
      int i2 = Integer.parseInt(line2);
      System.out.println("和は:" + (i1+i2));
    } catch (IOException e) {
      System.out.println(e);
    }   
  }
}
