//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2015-12-01

public class TestGeometricObject {
  public static void main(String[] args) {
    GeometricObject geoObject1 = new Circle(5);
    GeometricObject geoObject2 = new Rectangle(5, 3);
    System.out.println("The two objects have the same area ? " +
        equalArea(geoObject1, geoObject2));
    displayGeometricObject(geoObject1);
    displayGeometricObject(geoObject2);
  }

  public static boolean equalArea(GeometricObject object1, GeometricObject object2) {
    // Write your code here
    if(object1.getArea() != object2.getArea()) {
      return false;
    }
    return true;
  }

  public static void displayGeometricObject(GeometricObject object) {
    System.out.println();
    System.out.println("The area is " + object.getArea());
    System.out.println("The perimeter is " + object.getPerimeter());
  }
}
