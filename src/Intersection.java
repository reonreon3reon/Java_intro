//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-10-20 

import java.io.*;

public class Intersection
{
  public static int[] intersection(int myArrayA[], int myArrayB[]){
    int tmp[] = new int[10]; 
    int k = 0;
    for (int i = 0; i < myArrayA.length; i++) {
      for (int j = 0; j < myArrayB.length; j++) {
        if(myArrayA[i] == myArrayB[j]){
          tmp[k] = myArrayA[i];
          k++;
        }
      } 
    } 
    int intsec[] = new int[k];
    for(int i = 0; i < k; i++){
      intsec[i] = tmp[i];
    }
    return intsec; 
  }

  public static void main(String[] args)
  {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    int myArrayA[], myArrayB[], result[];

    try {
      myArrayA = new int[10];
      myArrayB = new int[10];
      System.out.println("myArrayA:");
      for (int i = 0; i < myArrayA.length; i++) {
        String line = br.readLine();
        myArrayA[i] = Integer.parseInt(line);
      }
      System.out.println("myArrayB:");
      for (int i = 0; i < myArrayB.length; i++) {
        String line = br.readLine();
        myArrayB[i] = Integer.parseInt(line);
      }
      System.out.println("Result:");
      result = intersection(myArrayA, myArrayB);
      for (int i = 0; i < result.length; i++){
        System.out.println(result[i]);
      }
    }
    catch (IOException e){
    }
  } 
}
