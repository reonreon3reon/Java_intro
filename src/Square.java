//  -*- coding: utf-8 -*- 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-10-13 

public class Square {
  public static void main(String[] args) {
    for(int i=1; i <= 11;i+=2){
      System.out.println(i + "の2乗は" + sq(i) + "です．");
    }
  }
  public static int sq(int n) {
    return n*n;
  }
}
