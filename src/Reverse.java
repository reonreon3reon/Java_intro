import java.io.*;

public class Reverse {
  public static void main(String[] args) {
    System.out.println("文字列を入力してください。");
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      String line = reader.readLine();
      char[] cs = new char[100];
      /* for (int i = 0;i < line.length(); i++){ */
      for (int i = line.length(); i > 0; i--) {
        char c = line.charAt(i-1);
        /* System.out.println( (line.length() - i) + " " + c); */
        cs[line.length() - i] = c;
      }
      System.out.print("\"" + line + "\"" + "のさかさまは" + "\"");
      System.out.print(cs);
      System.out.println("\"");
    } catch (IOException e) {
      System.out.println(e);
    }
  }
}
