//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2015-12-15

import java.io.*;
import java.util.*;

public class Join {
  public static void main(String[] args) throws IOException {
    int value;
    int count = 0;

    DataInputStream disR = new DataInputStream(new FileInputStream("R"));
    DataInputStream disS = new DataInputStream(new FileInputStream("S"));
    List<Integer> listR = new LinkedList<Integer>();
    List<Integer> listS = new LinkedList<Integer>();

    while (disR.available() > 0) {
      value = disR.readInt();
      listR.add(value);
    }

    while (disS.available() > 0) {
      value = disS.readInt();
      listS.add(value);
    }

    disR.close();
    disS.close();

    for (Iterator<Integer> iterR = listR.iterator(); iterR.hasNext();) {
      int rval = iterR.next();
      for (Iterator<Integer> iterS = listS.iterator(); iterS.hasNext();) {
        int sval = iterS.next();
        if (rval == sval) {
          count++;
        }
          }
        }
    System.out.println("Number of Matches: " + count);
  }
}
