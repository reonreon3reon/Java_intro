//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2015-10-13

/* 負で無い整数を２つ入力し， 再帰呼び出しを使ってアッカーマン関数の値を求めるプログラム(クラス名はAck)を作成しなさい． なお，アッカーマン関数とは，非負整数mとnに対し */
/* ack(0, n) 	= n + 1 	 */
/* ack(m, 0) 	= ack(m - 1, 1) 	　(m > 0) */
/* ack(m, n) 	= ack(m - 1, ack(m, n - 1)) 	　(m > 0, n > 0) */
/* と定義される関数である．この関数は，ループと固定された個数の変数だけでは計算できないことが知られている(再帰呼び出しや，スタックなど上限のない量のメモリが必要となる)．  */

import java.io.*;

public class Ack {
  public static int ack(int m, int n){
    if(m == 0){
      return n+1;
    }else if((n == 0) && (m > 0)){
      return ack(m - 1, 1);
    }else if((m > 0) && (n > 0)){
      return ack(m - 1, ack(m, n - 1));
    }
    return 0;
  }
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      String line;
      line = reader.readLine();
      int a = Integer.parseInt(line);
      line = reader.readLine();
      int b = Integer.parseInt(line);
      System.out.println("ack(" + a +", " + b + ") = "+ ack(a, b));
    } catch(IOException e){
      System.out.println(e);
    } catch(NumberFormatException e){
      System.out.println("数字の形式が正しくありません．");
    }
  }
}
