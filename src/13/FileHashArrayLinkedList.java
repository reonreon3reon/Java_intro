//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2016-01-19

class FileHashArrayLinkedList {
  // Please write your code here

  static void HashSearch(String s, ListString HashTable[]) {
    // Please write your code here
    int q = String2Integer(s);
    int hashed_q = HashFunction(q);
    ListString list_p = HashTable[hashed_q];
    ListString.Search(s, list_p);
  }

  static int String2Integer(String s) {
    int result = 0;

    result = (int)s.charAt(0);

    return result;
  }

  // Note that SzBkt is a static variable
  static int SzBkt = 10000;
  static int HashFunction(int l) {
    // Please write your code here
    return l % SzBkt;
  }

  public static void main (String[] args) {
    ListString [] HashTable = new ListString[SzBkt];
    ListString data;
    ListString head;

    for (int i = 0; i < SzBkt; i++) {
      HashTable[i] = null;
    }

    // Constructing a hash structure
    head = ListString.FileRead();
    for (data = head; data != null; data = data.next) {
      int s2int = String2Integer(data.name);
      int pos = HashFunction(s2int);
      HashTable[pos] = ListString.Insert(data.name, HashTable[pos]);
    }

    // Search
    long start, end;
    System.out.println("Invoking list:");
    start = System.currentTimeMillis();
    for (data = head; data != null; data = data.next) {
      ListString.Search(data.name, head);
        }
    end = System.currentTimeMillis();
    System.out.println("Time for list: " + (end - start) + " ms");

    System.out.println("Invoking hash:");
    start = System.currentTimeMillis();
    for (data = head; data != null; data = data.next) {
      HashSearch(data.name, HashTable);
    }
    end = System.currentTimeMillis();
    System.out.println("Time for hash: " + (end - start) + " ms");
  }
}
