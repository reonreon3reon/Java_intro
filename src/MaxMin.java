//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-10-20 


import java.io.*;

public class MaxMin 
{
  public static void maxmin(int myArray[]){
    int min = 100000;
    int max = -1;
		int max_i = 0;
    int min_i = 0;
    for (int i = 0; i < myArray.length; i++) {
			if(max < myArray[i]){
				max = myArray[i];
			  max_i = i;
		  }
      if(min > myArray[i]){
				min = myArray[i]; 
			  min_i = i;
      }
    } 
    System.out.println("Max: myArray["+max_i + "] " + max);
    System.out.println("Min: myArray["+min_i + "] " + min);
  }


  public static void main(String[] args)
  {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    int[] myArray;

    try {
      myArray = new int[10];
      System.out.println("Input 10 values");
      for (int i = 0; i < myArray.length; i++) {
        String line = br.readLine();
        myArray[i] = Integer.parseInt(line);
      }
      maxmin(myArray);
    }
    catch (IOException e){
    }
  }                                                                                                                    
}   
