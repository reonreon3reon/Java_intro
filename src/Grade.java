/* 100点満点の点数を入力し、 50点以上であれば、A、50点未満であれば、B、0から100以外の 数字であれば、errorと出力するプログラ */

import java.io.*;

public class Grade {
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      String line = reader.readLine();
      int n = Integer.parseInt(line);
      if (n >= 50 && 100 >= n) {
        System.out.println("A");
      } else if(n >= 0 && 50 > n){
        System.out.println("B");
      } else{
        System.out.println("ERROR");
      }
    } catch (IOException e) {
      System.out.println(e);
    } catch (NumberFormatException e) {
      System.out.println("数字の形式が正しくありません。");
    }
  }
}
