#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 :
#
# Author:   Ashigirl96
# URL:      http://pwn.hatenablog.com/
# License:  MIT License
# Created:  2015-11-24
#
from abc import ABCMeta, abstractmethod

class Player(metaclass=ABCMeta):
    @abstractmethod
    def play():
        pass

    def loop(n: int) -> None:
        for i in range(n):
            play()
