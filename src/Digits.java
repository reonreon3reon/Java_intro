//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-07-28 


import java.io.*;

public class Digits {
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      System.out.println("正の整数を入力して下さい．");
      String line = reader.readLine();
      int q = Integer.parseInt(line);
      int n = q, i;
      for(i = 0;n != 0;i++){
        n /= 10;
      }
      System.out.println(q + "は" + i + "桁の数です．");
    }
    catch (IOException e) {
      System.out.println(e);
    } catch (NumberFormatException e) {
      System.out.println("数字の形式が正しくありません。");
    }
  }
}
