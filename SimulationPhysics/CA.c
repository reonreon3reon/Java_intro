#define COUNT 5
#define LEN 8
#include<stdio.h>


int cellAutomaton(int state) {
  int i;
  int states[8] = {111, 110, 101, 100, 11, 10, 1, 0};
  int converted[8] = {1, 0, 1, 1, 1, 0, 0, 0};
  for(i = 0; i < 8; i ++ ) {
    if (state == states[i]) {
      return converted[i];
    }
  }
  return -1;
}

int convert(int q, int w, int e) {
  return q*100 + w*10 + e;
}

int main(void) {
  int cell[LEN] = {1, 0, 1, 0, 1, 1, 0, 1};
  int new_cell[LEN] = {0};
  int i, j;
  int new_s;
  for(i = 0; i < COUNT; i++) {
    for(j = 0;j < LEN; j++) {
      new_s = convert(cell[(LEN-1+j)%LEN], cell[j], cell[(j+1)%LEN]);
      new_cell[j] = cellAutomaton(new_s);
    }
    // print & assign.
    for(j = 0;j < LEN; j++) {
      printf("%d", new_cell[j]);
      cell[j] = new_cell[j];
    }
    printf("\n");
  }

  return 0;
}
