#define SEED 149
#define PI 3.14159
#define MAX 10
#define H 3.5
#define org 50
#define dt 0.1
#define nk 20

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

double p(double v) {
  return pow(v, 2);
}


void write_csv(double (*x)[2], char *filename) {
  FILE *fo;
  char *fname = filename;
  int i = 0;

  // touch a file.
  if ( (fo = fopen(fname, "w")) == NULL ) {
    printf("File[%s] dose not open!! \n", fname);
    exit(0);
  }

  // generate rand pair and write into below file.
  for(i = 0; i < 500; i++) {
    fprintf(fo, "%f,%f\n", x[i][0], x[i][1]);
  }
}

int read_csv(double (*x)[2], char *filename) {
  FILE *fp;
  char *fname = filename;
  double n1, n2;
  int ret, i = 0;

  fp = fopen( fname, "r" );
  if( fp == NULL ){
    printf( "%s cannot open\n", fname );
    return -1;
  }

  while( ( ret = fscanf( fp, "%lf,%lf", &n1, &n2) ) != EOF ){
    x[i][0] = n1;
    x[i][1] = n2;
    i++;
  }

  fclose( fp );
  return 0;
}

void set_speed(double (*x1)[2], double (*x2)[2]) {
  int i;
  for(i = 0; i < 500; i++) {
    x1[i][0] = H*x2[i][0];
    x1[i][1] = H*x2[i][1];
  }
}

void translate_quadrant(double (*x1)[2], double (*x2)[2]) {
  int i;
  for(i = 0; i < 500; i++) {
    x1[i][0] = x2[i][0] + org;
    x1[i][1] = x2[i][1] + org;
  }
}


double sum(double (*x1)[2], int q) {
  int s=0, i;
  for(i = 0; i < 500; i++) {
    s += x1[i][q];
  }
  return s;
}
void new_coordination(double (*x1)[2], double (*x2)[2]) {
  int i, k;
  for(k = 0; k < nk; k++) {
    for(i = 0; i < 500; i++) {
      x1[i][0] +=  x2[i][0] * dt;
      x1[i][1] +=  x2[i][1] * dt;
    }
  }
}

int main(void) {

  // 1. init star coordinate.
  int i;
  double vxy[500][2] = {0.0};
  double xy[500][2] = {0.0};
  double xy0[500][2] = {0.0};
  read_csv(xy0, "b.csv");

  // 2. init first star speed.
  set_speed(vxy, xy0);

  // 3. parallel translation.
  translate_quadrant(xy, xy0);
  write_csv(xy, "before.csv");

  // 4. Calculate new star coordinate.
  new_coordination(xy, vxy);

  // 5. write after expantion
  write_csv(xy, "after.csv");
  return 0;
}
