#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 :
#
# Author:   Ashigirl96
# URL:      http://pwn.hatenablog.com/
# License:  MIT License
# Created:  2016-01-07

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
tips = sns.load_dataset("tips")

before = np.array(pd.read_csv('./before.csv'))
after = np.array(pd.read_csv('./after.csv'))
bx, by = before[:,0], before[:,1]
ax, ay = after[:,0], after[:,1]
plt.figure(num=None, figsize=(8, 8), dpi=80, facecolor='w', edgecolor='k')
plt.scatter(bx, by)
plt.scatter(ax, ay)
plt.xlabel('x')
plt.ylabel('y')
plt.title(u"""$x^2 + y^2 \leq 5$""")
plt.savefig('plot2.png')
