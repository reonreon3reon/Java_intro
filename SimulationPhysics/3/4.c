#define G 1
#define dx 1
#define nm 2
#define ni 100

#include<stdio.h>

int main(void) {
  double p1 = 0, p2 = 0, phi[4][4] = {0.0}, ro[4][4] = {0.0};
  phi[1][3] = 22.5;
  phi[2][3] = 36.0;
  phi[3][1] = -4.5;
  phi[3][2] =  9.0;
  int ix = 0, iy = 0, i, j;

  for(ix = 0; ix <= nm + 1; ix++) {
    for(i = 1; i <= ni; i++) {
      for(ix = 1;ix <= nm; ix++)
      for(iy = 1; iy <= nm; iy++) {
        p1 = phi[ix+1][iy] + phi[ix-1][iy]+ \
                phi[ix][iy+1] + phi[ix][iy-1];
        ro[ix][iy] = 6*ix - 3*iy;
        p2 = G*ro[ix][iy]*dx*dx; // G: const
        phi[ix][iy] = p1/4 - p2/4;
      }
    }
  }
  for(i = 1;i < 3;i++){
    for(j = 1;j < 3;j++){
      printf("%.3lf ", phi[i][j]);
    }printf("\n");
  }
  return 0;
}
