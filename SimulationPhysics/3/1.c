#define SEED 149
#define PI 3.14159
#define MAX 10

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

double p(double v) {
  return pow(v, 2);
}

void genRandPairWrite(void) {
  FILE *fo;
  char *fname = "b.csv";
  int i = 0;
  double r[500] = {0.0};
  double s[500] = {0.0};

  // touch a file.
  if ( (fo = fopen(fname, "w")) == NULL ) {
    printf("File[%s] dose not open!! \n", fname);
    exit(0);
  }
  // set seed.
  srand(SEED);

  // generate rand pair and write into below file.
  while(r[500-1] == 0) {
    s[i] = 10*(double)rand()/RAND_MAX-5;
    r[i] = 10*(double)rand()/RAND_MAX-5;
    if (p(s[i]) + p(r[i]) <= p(5)) {
      fprintf(fo, "%f,%f\n", r[i], s[i]);
      i++;
    }
  }
}

int main(void) {
  genRandPairWrite();
  return 0;
}
