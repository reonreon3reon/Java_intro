#define SEED 149
#define PI 3.14159
#define MAX 10

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

void genRandPairWrite(void) {
  FILE *fo;
  char *fname = "data3.csv";
  int i = 0;
  double r[500] = {0.0};
  double s[500] = {0.0};

  // touch a file.
  if ( (fo = fopen(fname, "w")) == NULL ) {
    printf("File[%s] dose not open!! \n", fname);
    exit(0);
  }
  // set seed.
  srand(SEED);

  // generate rand pair and write into below file.
  while(r[500-1] == 0) {
    s[i] = 2*(rand()/(RAND_MAX + 1.0)) - 1;
    r[i] = 2*(rand()/(RAND_MAX + 1.0)) - 1;
    if (s[i]*s[i] + r[i]*r[i] <= 1) {
      fprintf(fo, "%f,%f\n", r[i], s[i]);
      i++;
    }
  }
}
double area(int num, int seed) {
  // set seed.
  srand(SEED + seed);
  int i = 0;
  double r[num];
  double s[num];
  int hit = 0;

  for(;i < num; i++) {
    s[i] = 2*(rand()/(RAND_MAX + 1.0)) - 1;
    r[i] = 2*(rand()/(RAND_MAX + 1.0)) - 1;
    if (s[i]*s[i] + r[i]*r[i] <= 1) {
      hit++;
    }
  }
  return (double)4*hit/num;
}


int main(void) {
  genRandPairWrite();
  int i, j;
  double allArea, meanArea;
  int pairs[3] = {100, 1000, 10000};
  for(int i=0;i < 3; i++) {
    allArea = 0.0;
    for(int j=1;j <= MAX;j++) {
      allArea += area(pairs[i], j);
      printf("%f \n", area(pairs[i], j));
    }
    meanArea = allArea/MAX;
    printf("%f\n",meanArea);
    printf("%f\n",(PI-meanArea)/PI);
    printf("---------------\n");
  }
  return 0;
}
