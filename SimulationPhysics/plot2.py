#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 :
#
# Author:   Ashigirl96
# URL:      http://pwn.hatenablog.com/
# License:  MIT License
# Created:  2016-01-09

import numpy as np
import matplotlib.pyplot as plt
from numpy.random import randint

def hit_miss(num):
    s = randint()
    r = randint()
    hit = 0
    for i in range(num):
        if s**2 + r**2 <= 1:
            hit+=1
            print("Hit: ", hit)
        else:
            print("Miss: ", hit)
            continue
    return hit*1./num

for i in [100, 1000, 10000]:
    print(hit_miss(i))
