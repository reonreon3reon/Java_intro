#define SEED 149
#define PI 3.14159
#define MAX 10
#define H 3.5
#define org 50
#define dt 0.1
#define nk 40
#define nm 99
#define M 1
#define ni 50
#define G 1
#define dx 1
#define star_size 500

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double p(double v) {
  return pow(v, 2);
}

typedef struct{
  double x, y;
  double vx, vy;
} Star;

void write_csv(char *filename, Star *s) {
  FILE *fo;
  char *fname = filename;
  int i = 0;

  // touch a file.
  if ( (fo = fopen(fname, "w")) == NULL ) {
    printf("File[%s] dose not open!! \n", fname);
    exit(0);
  }

  // generate rand pair and write into below file.
  for(i = 0; i < star_size; i++) {
    fprintf(fo, "%f,%f\n", s[i].x, s[i].y);
  }
}

int main(){
  srand(SEED);

  Star stars[star_size];
  int x, y;
  double Fpx, Fpy;
  double phi[nm+2][nm+2] = {0.0};
  double ro[nm+2][nm+2] = {0.0};
  double Fx[nm+1][nm+1] = {0.0};
  double Fy[nm+1][nm+1] = {0.0};


  for(int i = 0; i<star_size;){
    double s = 10*(double)rand()/RAND_MAX-5;
    double r = 10*(double)rand()/RAND_MAX-5;
    if (p(s) + p(r) <= p(5)) {
      stars[i].x = s+org;
      stars[i].y = r+org;
      stars[i].vx = s * H;
      stars[i].vy = r * H;
      i++;
    }
  }
  // $BKDD%A0$N@1(B
  write_csv("before.csv", stars);
  for (int q; q < nk; q++){
    // $BKDD%8e$N%k!<%W?t$,(B20$B$N$H$-$N@1(B
    if( q==20 ) {
      write_csv("after20.csv", stars);
    }

    for(int x=0; x <  nm+2; x++) {
      for(int y=0; y < nm+2; y++){
        ro[x][y] = 0.0;
      }
    }
    for(int i = 0; i < star_size; i++){
      int x = stars[i].x+0.5;
      int y = stars[i].y+0.5;
      if(0 <= x && x < nm+2 && 0<=y && y<nm+2)
      ro[x][y] += 1.0;
    }

    // $B%,%&%9%6%$%G%kK!(B
    for(int i=1; i <= ni; i++){
      for(int ix=1;ix <= nm; ix++) {
        for(int iy=1; iy <= nm; iy++){
          double p1 = phi[ix+1][iy]+phi[ix-1][iy] + \
                      phi[ix][iy+1]+phi[ix][iy-1];
          double p2 = G*ro[ix][iy]*dx*dx;
          phi[ix][iy] = p1/4-p2/4;
        }
      }
    }

    // $B=ENO>l(BFx, Fy $B$K$D$$$F7W;;(B
    for(int x=0; x < nm+1; x++)
    for (int y=0; y < nm+1; y++){
      Fx[x][y] = -(phi[x+1][y]-phi[x][y])/dx;
      Fy[x][y] = -(phi[x][y+1]-phi[x][y])/dx;
    }
    for(int i = 0; i < star_size; i++){
      x = round(stars[i].x); // $B;M<N8^F~(B
      y = round(stars[i].y); // $B;M<N8^F~(B
      Fpx = M * Fx[x][y];
      Fpy = M * Fy[x][y];
      stars[i].vx += (Fpx/M)*dt;
      stars[i].vy += (Fpy/M)*dt;

      stars[i].x += stars[i].vx*dt;
      stars[i].y += stars[i].vy*dt;
    }
  }

  // $BKDD%8e$N%k!<%W?t$,(B40$B$N$H$-$N@1(B
  write_csv("after40.csv", stars);

  return 0;
}
