#define SEED 149
#define PI 3.14159
#define MAX 10
#define H 3.5
#define org 50
#define dt 0.1
#define nk 40
#define nm 99
#define M 1
#define ni 50
#define G 1
#define dx 1

#define star_size 500
#define rep(i, q) for(int i = 0; i < q; i++)
#define rep1(i, q) for(int i = 1; i < q; i++)

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

double p(double v) {
  return pow(v, 2);
}

typedef struct {
  double x;
  double y;
} F;

void gauss_zaidel(double phi[][nm], int ro[][nm], double xy[][2], F f[][nm]) {
  int ix = 0, iy = 0, i, j, p1 = 0, p2 = 0;
  int tmp_x, tmp_y;
  rep(ix, nm+2) {
    rep(i, nm+1) {
      phi[ix][iy] = 0;
    }
  }
  rep1(ix, nm+1) rep1(iy, nm+1) {
    p1 = phi[ix+1][iy] + phi[ix-1][iy]+ \
    phi[ix][iy+1] + phi[ix][iy-1];
    // 6. calc ro by NGP method
    rep(j, star_size) {
      tmp_x = round(xy[j][0]);
      tmp_y = round(xy[j][1]);
      ++ro[tmp_x][tmp_y];
    }
    //////////////////////////
    p2 = G*ro[ix][iy]*dx*dx; // G: const
    phi[ix][iy] = p1/4 - p2/4;
    /* Fx[ix][iy] = -(phi[ix+1][iy]-phi[ix][iy])/dx; */
  }
  rep(x, nm+1) rep(y, nm+1) {
    f[ix][iy].x = -(phi[ix+1][iy]-phi[ix][iy])/dx;
    f[ix][iy].y = -(phi[ix][iy+1]-phi[ix][iy])/dx;
  }
}

void init_ro(int ro[][nm]) {
  int i, j;
  for(i = 0; i < nm; i++ ) {
    for(j = 0; j < nm; j++ ) {
      ro[i][j] = 0;
    }
  }
}


void write_csv(double (*x)[2], char *filename) {
  FILE *fo;
  char *fname = filename;
  int i = 0;

  // touch a file.
  if ( (fo = fopen(fname, "w")) == NULL ) {
    printf("File[%s] dose not open!! \n", fname);
    exit(0);
  }

  // generate rand pair and write into below file.
  for(i = 0; i < star_size; i++) {
    fprintf(fo, "%f,%f\n", x[i][0], x[i][1]);
  }
}

int read_csv(double (*x)[2], char *filename) {
  FILE *fp;
  char *fname = filename;
  double n1, n2;
  int ret, i = 0;

  fp = fopen( fname, "r" );
  if( fp == NULL ){
    printf( "%s cannot open\n", fname );
    return -1;
  }

  while( ( ret = fscanf( fp, "%lf,%lf", &n1, &n2) ) != EOF ){
    x[i][0] = n1;
    x[i][1] = n2;
    /* printf("%lf %lf\n", x[i][0], x[i][1]); */
    i++;
  }

  fclose( fp );
  return 0;
}

void set_speed(double (*x1)[2], double (*x2)[2]) {
  int i;
  for(i = 0; i < star_size; i++) {
    x1[i][0] = H*x2[i][0];
    x1[i][1] = H*x2[i][1];
  }
}

void translate_quadrant(double (*x1)[2], double (*x2)[2]) {
  int i;
  for(i = 0; i < star_size; i++) {
    x1[i][0] = x2[i][0] + org;
    x1[i][1] = x2[i][1] + org;
  }
}


double sum(double (*x1)[2], int q) {
  int s=0, i;
  for(i = 0; i < star_size; i++) {
    s += x1[i][q];
  }
  return s;
}

void new_coordination(double (*xy)[2], double (*vxy)[2], F f[][nm], F fp[][nm]) {
  int i, k;
  rep(i, nm) {
    rep(j, nm) {
      fp[i][j].x = M*f[i][j].x;
      fp[i][j].y = M*f[i][j].y;
    }
    /* printf("\n"); */
  }
  int r_x, r_y;
  rep1(i, star_size) {
    r_x = round(xy[i][0]);
    r_y = round(xy[i][1]);
    vxy[i][0] += (fp[r_x][r_y].x / M ) * dt;
    vxy[i][1] += (fp[r_x][r_y].y / M ) * dt;
    printf("vxy: %lf %lf\n", vxy[i][0], vxy[i][1]);
    //
    xy[i][0] +=  vxy[i][0] * dt;
    xy[i][1] +=  vxy[i][1] * dt;
    printf("%d - xy: %lf %lf\n", i, xy[i][0], xy[i][1]);
  }
}

int main(void) {

  // 1. init star coordinate.
  int i,j;
  double vxy[star_size][2] = {0.0};
  double xy[star_size][2] = {0.0};
  double xy0[star_size][2] = {0.0};
  read_csv(xy0, "b.csv");

  // Conform.
  /* for(i = 0; i < star_size; i++) { */
  /*   printf("%lf %lf\n", xy0[i][0], xy0[i][1]); */
  /* } */

  // 2. init first star speed.
  set_speed(vxy, xy0);

  // 3. parallel translation.
  translate_quadrant(xy, xy0);
  write_csv(xy, "before.csv");


  // 4. init phi, ro
  double phi[nm][nm] = {0.0};
  F f[nm][nm], fp[nm][nm];
  int ro[nm][nm] = {0}, Fp;
  int tmp_x, tmp_y;

  // 5. loop nk times
  rep(i, nk) {
    init_ro(ro);

    // 7. calc Potential by Gauss-Zaidel method
    // 8. calc grafity place
    gauss_zaidel(phi, ro, xy, f);

    // 9. moving stars.
    new_coordination(xy, vxy, f, fp);

  }

  // 6. write after expantion
  write_csv(xy, "after.csv");
  return 0;
}
