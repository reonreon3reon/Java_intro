#include<stdio.h>

#define X 5
#define Y 2
#define size 4

int main(void) {
  double A[size][size] = {{ 4, -1, 0, 0},
                         { -1, 4, -1, 0},
                         { 0, -1, 4, -1},
                         { 0, 0, -1, 4}};
 double B[size][size] = {{ -1, 0, 0, 0},
                        { 0, -1, 0, 0},
                        { 0, 0, -1, 0},
                        { 0, 0, 0, -1}};
  int i, j;
  for(i = 0; i < X; i++) {
    for(j = 0; j < Y; j++) {
      printf("%d ",A[i][j]);
    } printf("\n");
  }
}
