using System;

class CommentSample {
  static void Main() {
    var dataSource = new[] {
      455,  58,   8,   7, 987,  56,   2,  64, 698,  79,
      98,  79,  45, 465, 167,  97,  94, 657, 237, 587,

    };
    double mean;
    double variance;

    CalcMean(dataSource, out mean, out variance);

    Console.WriteLine("Mean: {0}, Variance: {1}", mean, variance);
  }
  static void CalcMean(int[] data, out double mean, out double vari) {
    int sum = 0;
    int sq_sum = 0;

    foreach(int i in data) {
      sum += i;
      sq_sum += i*i;
    }
    // Compute mean and var
    mean = (double)sum / data.Length;
    vari = (double)sq_sum / data.Length - mean*mean;
  }
}
