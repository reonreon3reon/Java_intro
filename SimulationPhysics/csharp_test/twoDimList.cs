//  -*- coding: utf-8 -*-
//  vim: set fileencoding=utf-8 :
//  StudentNumber: s1511392
//  Author:   Reon Nishimura
//  Created:  2016-01-06
using System;

public class twoDimList {
  static void Main() {
    double[,] a = new double[,] {{1,2}, {2,1}, {0,1}};
    double[,] b = new double[,] {{1,2,0}, {0,1,2}};
    double[,] c = new double[3, 3];

    for(int i=0; i<a.GetLength(0); i++) {
      for (int j=0; j<b.GetLength(1); j++) {
        c[i,j] = 0;
        for (int k=0; k<a.GetLength(1); k++) {
          c[i,j] = a[i,k] * b[k,j];
          Console.Write("{0}, ",c[i,j]);
        }
      }
      Console.WriteLine();
    }
  }
}
