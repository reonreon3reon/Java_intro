using System;

class List {
  // static void /// <summary>
  ///   The main entry point for the application
  /// </summary>
  // [STAThread]
  public static void Main(string[] args)
  {
    int[] a = new int[5];
    for(int i=0; i < a.Length; i++) {
      a[i] = int.Parse(Console.ReadLine());
    }

    //Compute the value
    int square_sum = 0;
    for(int i=0; i < a.Length;i++) {
      square_sum += a[i]*a[i];
    }

    // Output
    Console.WriteLine("{0}", square_sum);

  }
}
