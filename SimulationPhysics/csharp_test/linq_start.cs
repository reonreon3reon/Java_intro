using System;
using System.Linq;

class LineTest{
  // public void /// <summary>
  // ///   The main entry point for the application
  // /// </summary>
  // [STAThread]
  public static void Main(string[] args) {
    var studentDict =
    new [] {
      new { studentNum = 14, farName = "1", surName = "10"},
      new { studentNum = 21, farName = "2", surName = "13"},
      new { studentNum = 22, farName = "3", surName = "14"},
      new { studentNum = 19, farName = "4", surName = "15"},
      new { studentNum = 18, farName = "5", surName = "16"},
      new { studentNum = 16, farName = "6", surName = "17"},
      new { studentNum =  8, farName = "7", surName = "19"},
      new { studentNum = 28, farName = "8", surName = "18"},
    };

    var studentNumRollfirsthalf =
      from p in studentDict
      where p.studentNum <= 15
      orderby p.studentNum
      select p.surName;

    foreach(var name in studentNumRollfirsthalf) {
      Console.WriteLine("{0}", name);
    }
  }
}
