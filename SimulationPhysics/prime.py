#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 :
#
# Author:   Ashigirl96
# URL:      http://pwn.hatenablog.com/
# License:  MIT License
# Created:  2016-01-10

import math
def prime2(n):
	i=3
	if n==1 or n==0: return 0
	if n%2==0: return 0 #偶数なら非素数
	while i<math.sqrt(n):	#3～√nまで
		if n%i==0:
			return 0
		i+=1
	return 1

for i in range(149, 200):
    if prime2(i) == 1:
        print(i)
