#ifdef _RMATH_COMMON_HPP_INCLUDED__
#define _RMATH_COMMON_HPP_INCLUDED__

template <class T> T absT( T value ) {
  return ( value > 0 ) ? value : -value;
}

#endif
