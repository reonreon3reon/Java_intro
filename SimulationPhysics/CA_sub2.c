#define COUNT 20
#define LEN 20
#define SEED 149

#include<stdio.h>
#include<stdlib.h>

// 184 regulation
int cellAutomaton(int state) {
  int i;
  int states[8] = {111, 110, 101, 100, 11, 10, 1, 0};
  int converted[8] = {1, 0, 1, 1, 1, 0, 0, 0};
  for(i = 0; i < 8; i ++ ) {
    if (state == states[i]) {
      return converted[i];
    }
  }
  return -1;
}

// generate random array
void genRandomArray(int* array, int size, double init) {
  int i;
  int ele;
  while(1) {
    for(i = 0; i < size; i++ ) {
      ele = rand()%10;
      array[i] = ele < init*10 ? 1 : 0;
    }
    // validation.
    int count = 0;
    for(i = 0; i < size; i++ ) {
      count += array[i];
    }
    if( count == LEN*init ) {
      break;
    }
  }
}

int convert(int q, int w, int e) {
  return q*100 + w*10 + e;
}

int main(void) {
  int cell[LEN] = {0};
  int new_cell[LEN] = {0};
  int i, j, k;
  int new_s;
  double init_density[3] = {0.3, 0.5, 0.7};
  // set srand.
  srand(SEED);

  for(k = 0; k < 3; k++) {
    printf("init=%.1f\n", init_density[k]);
    genRandomArray(cell, LEN, init_density[k]);
    for(i = 0; i < COUNT; i++) {
      for(j = 0;j < LEN; j++) {
        new_s = convert(cell[(LEN-1+j)%LEN], cell[j], cell[(j+1)%LEN]);
        new_cell[j] = cellAutomaton(new_s);
      }
      // print & assign.
      printf("t=%02d ", i);
      for(j = 0;j < LEN; j++) {
        printf("%d", new_cell[j]);
        cell[j] = new_cell[j];
      }
      printf("\n");
    }
    printf("---------------------\n");
  }
  return 0;
}
