#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 :
#
# Author:   Ashigirl96
# URL:      http://pwn.hatenablog.com/
# License:  MIT License
# Created:  2016-01-07

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
tips = sns.load_dataset("tips")

data = np.array(pd.read_csv('./data3.csv'))
s, r = data[:,0], data[:,1]
plt.figure(num=None, figsize=(8, 8), dpi=80, facecolor='w', edgecolor='k')
plt.scatter(s, r)
plt.xlabel('s')
plt.ylabel('r')
plt.title(u"""$s^2 + r^2 \leq 1$""")
plt.savefig('plot.png')
