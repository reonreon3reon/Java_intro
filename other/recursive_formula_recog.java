import java.io.*;

public class recursive_formula_recog {
    public static void main(String[] args) {
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	try {
	    while(true) {
		System.out.print("input: ");
		String line = reader.readLine();
		if (line == null) {
		    break;
		}
		double result = f(line);
		System.out.println("result: " + result);
	    }
	} catch (IOException e) {
	    System.out.println(e);
	} catch (ArithmeticException e) {
	    System.out.println(e);
	}
    }

    static double f (String s) {
	int count = 0;
	for (int i = 0; i < s.length(); i++) {
	    if (count == 0) {
		if (s.charAt(i) == '+') {
		    return f(s.substring(0, i)) + f(s.substring(i + 1));
		} else if (s.charAt(i) == '-') {
		    return f(s.substring(0, i)) - f(s.substring(i + 1));
		}
	    }
	    if (s.charAt(i) == '(') {
		count++;
	    } else if (s.charAt(i) == ')') {
		count--;
	    }
	}
	for (int j = s.length() - 1; j >  -1; j--) {
	    if (count == 0) {
		if (s.charAt(j) == '*') {
		    return f(s.substring(0, j)) * f(s.substring(j + 1));
		} else if (s.charAt(j) == '/') {
		    return (double)(f(s.substring(0, j)) / f(s.substring(j + 1)));
		} else if (s.charAt(j) == '%') {
		    return f(s.substring(0, j)) % f(s.substring(j + 1));
		}
	    }
	    if (s.charAt(j) == '(') {
		count++;
	    } else if (s.charAt(j) == ')') {
		count--;
	    }
	}
	for(int k = 0; k < s.length(); k++) {
	    if (count == 0) {
		if (s.charAt(k) == '^') {
		    return Math.pow(f(s.substring(0, k)), f(s.substring(k + 1)));
		}
	    }
	}
	if (s.charAt(0) == '(') {
	    if (s.charAt(s.length() - 1) == ')') {
		return f(s.substring(1, s.length() - 1));
	    }
	}
	return Integer.parseInt(s);
    }
}
