#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 :
#
# Author:   Ashigirl96

#-3/2 == -2
abs2 = lambda x,y: int(x*1.0/y)
on_point_cut = lambda x: float(x[0:x.index('.')+3])

def factor(begin):
  global SyntaxFlag
  global i
  val = 0

  if begin[i] == "(":
    i+=1
    val = expression(begin)
    i+=1

  while begin[i].isdigit():
    SyntaxFlag=0
    val *= 10
    val += int(begin[i])
    i+=1
  return val

def mul(begin):

  global i
  val = factor(begin)

  while True:
    if begin[i] is "*":
      if begin[i+1] is "*":
        i += 2
        val **= mul(begin)#factor(begin)
      else:
        return val
    else:
      break
  return val

def term(begin):

  global i
  val = mul(begin) #factor(begin)

  while True:
    if begin[i] is "*":
      i += 1
      val *= mul(begin)
    elif begin[i] is "/":
      i += 1
      val2 = mul(begin)
      if val < 0 or val2 < 0:
        val = on_point_cut(str(abs2(val,val2)))
      else:
        val /= 1.0*val2
        val = on_point_cut(str(val))
    elif begin[i] is "%":
      i += 1
      val %= mul(begin)
    else:
      break

  return val

def expression(begin):

  global i
  val = term(begin)

  while True:
    if begin[i] is "+":
      i += 1
      val += expression(begin)
    elif begin[i] is "-":
      i += 1
      val -= expression(begin)
    else:
      break
  return val

def judge(begin):

  global i
  stack =[]
  while begin[i] != "~":
    val = expression(begin)

    # if begin[i] == "=":
    #   if begin[i+1] == "=":
    #     i+=2
    #     stack.append(val)
    #     stack.append('=')
    if begin[i:i+2] == "==":
        i+=2
        stack.append(val)
        stack.append('=')
    elif begin[i] == "!":
      i+=2
      stack.append(val)
      stack.append('!')
    elif begin[i] == ">":
      if begin[i+1] == "=":
        i+=2
        stack.append(val)
        stack.append('>=')
      else:
        i+=1
        stack.append(val)
        stack.append('>')
    elif begin[i] == "<":
      if begin[i+1] == "=":
        i+=2
        stack.append(val)
        stack.append('<=')
      else:
        i+=1
        stack.append(val)
        stack.append('<')
    else:
      stack.append(val)
      break
  if len(stack)==1:
    return val
  else:
    #return True if  sum(stack)*1.0/len(stack)==stack[0] else False
    #[50, '=', 10, '!', 5]
    val = True
    for x,y in enumerate(stack):
      if x%2:
        #print("y:",y,stack[x-1],stack[x+1])
        if y is "=":
          if (stack[x-1] == stack[x+1]) == False:
            val = False
            break
        elif y is "!":
          if (stack[x-1] != stack[x+1]) == False:
            val = False
            break
        elif ">" in y:
          if "=" in y:
            if (stack[x-1] >= stack[x+1]) == False:
              val = False
              break
          elif (stack[x-1] > stack[x+1]) == False:
            val =False
            break
        elif "<" in y:
          if "=" in y:
            if (stack[x-1] <= stack[x+1]) == False:
              val = False
              break
          elif (stack[x-1] < stack[x+1]) == False:
            val =False
            break
    return val


def main():
  while True:
    try:
      ex=""
      global SyntaxFlag
      SyntaxFlag=1
      tmp = (raw_input(">>")+"~").split()
      for j in tmp: ex+=j
      if True in [ex == x for x in ['exit~','quit~']]:
        print "End..."
        exit(0)
      elif ex=="~": continue
      global i
      i = 0
      #print expression(ex)
      result= judge(ex)
      if SyntaxFlag:
        raise SyntaxError,'WTF are u "d"oin!!?'
      else:
        print(result)
    except EOFError:
      break

if __name__ == '__main__':
  main()
