# This is Project of Programming Introduction with Java

## source

```bash
$cd src
$javac [file].java
```

## How to execute

```bash
$cd class
$java [file]
```
