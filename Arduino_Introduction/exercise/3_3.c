int Do = 956;
int Re = 851;
int Mi = 758;
int Fa = 716;
int So = 638;
int Ra = 568;
int Si = 506;
int DoU =478;
int ReU =426;
int B16 =125;
int B8 = B16*2;
int B4 = B16*4;
int B2 = B16*8;

int speaker = 12;

void sound (int tone_delay) {
  digitalWrite(speaker, HIGH);
  delayMicroseconds(tone_delay);
  digitalWrite(speaker, LOW); 
  delayMicroseconds(tone_delay);
}

void play (int tone, int length) {
  unsigned long tone_end_time;
  tone_end_time = millis() + length;

  while (millis() < tone_end_time) {
    sound(tone);
  } 
}

void rent (int length) {
  delay(length); 
}

void setup() {
  pinMode(speaker, OUTPUT); 
}

void loop() {
  BunBunBun();
}

void BunBunBun ()  {
  play(So, B4);
  play(Fa, B4);
  play(Mi, B2);
  play(Re, B8);
  play(Mi, B8);
  play(Fa, B8);
  play(Re, B8);
  play(Do, B4);

  rent(B4);

  play(Mi, B8);
  play(Fa, B8);
  play(So, B8);
  play(Mi, B8);
  play(Re, B8);
  play(Mi, B8);
  play(Fa, B8);
  play(Re, B8);


  play(Mi, B8);
  play(Fa, B8);
  play(So, B8);
  play(Mi, B8);
  play(Re, B8);
  play(Mi, B8);
  play(Fa, B8);
  play(Re, B8);

  play(So, B4);
  play(Fa, B4);
  play(Mi, B2);


  play(Re, B8);
  play(Mi, B8);
  play(Fa, B8);
  play(Re, B8);
  play(Do, B4); 

  rent(B4);

}
