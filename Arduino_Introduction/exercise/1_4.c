int ledRed = 10;
int ledOrange = 11;
int ledGreen = 12;

int Red_state;
int Orange_state;
int Green_state;

int Red_length = 1;
int Orange_length = 2;
int Green_length = 3;

int Red_count;
int Orange_count;
int Green_count;

void setup() {
  pinMode(ledRed, OUTPUT);
  pinMode(ledOrange, OUTPUT);
  pinMode(ledGreen, OUTPUT);
  Red_state = HIGH;
  Orange_state = HIGH;
  Green_state = HIGH;
  Red_count = 0;
  Orange_count = 0;
  Green_count  = 0; 
}

void loop() {
  digitalWrite(ledGreen, HIGH);
  sec_delay(3);
  digitalWrite(ledGreen, LOW);
  sec_delay(3);
  digitalWrite(ledRed, HIGH);
  sec_delay(1);
  digitalWrite(ledRed, LOW);
  sec_delay(1);
}

void sec_delay(int sec) {
  delay(sec*1000);
}

int check_time (int led_count, int led_length) {
  if ( led_count == led_length - 1) return 1;
  else return 0; 
}
