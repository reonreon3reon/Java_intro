int ledRed = 10;
int ledOrange = 11;
int ledGreen = 12;

int Red_state;
int Orange_state;
int Green_state;

int Red_length = 1;
int Orange_length = 2;
int Green_length = 3;

int Red_count;
int Orange_count;
int Green_count;

void setup() {
  pinMode(ledRed, OUTPUT);
  pinMode(ledOrange, OUTPUT);
  pinMode(ledGreen, OUTPUT);
  Red_state = HIGH;
  Orange_state = HIGH;
  Green_state = HIGH;
  Red_count = 0;
  Orange_count = 0;
  Green_count  = 0; 
}

void loop() {
  if (Green_state == HIGH) {
    digitalWrite(ledGreen, HIGH);
    Green_state = LOW;
  } else {
    digitalWrite(ledGreen, LOW);
    Green_state = HIGH;
  }
  sec_delay(3);
  if (Orange_state == HIGH) {
    digitalWrite(ledOrange, HIGH);
    Orange_state = LOW;
  } else {
    digitalWrite(ledOrange, LOW);
    Orange_state = HIGH;
  }
  sec_delay(1);
  if (Red_state == HIGH) {
    digitalWrite(ledRed, HIGH);
    Red_state = LOW;
  } else {
    digitalWrite(ledRed, LOW);
    Red_state = HIGH;
  }

  sec_delay(2);
}

void sec_delay(int sec) {
  delay(sec*1000);
}
