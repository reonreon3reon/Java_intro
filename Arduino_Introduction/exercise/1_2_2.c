int SWITCH = 13;
int ledPin = 12;
int SW_now;
int SW_last;

long lastDebounceTime = 0;
long debounceDelay = 50;
int LED_state;

void setup() {
  pinMode(SWITCH, INPUT);
  pinMode(ledPin, OUTPUT);
  LED_state = LOW;
}

void loop() {
  SW_now = digitalRead(SWITCH);
  if (SW_last == LOW && SW_now == HIGH) {
    int now = millis(); // set now time.
    if ((now - lastDebounceTime) > debounceDelay) {
      onPress(); // change LED state.
    }
    lastDebounceTime = now;
  }
  SW_last = SW_now;
}

void onPress() {
  if (LED_state == LOW) {
    digitalWrite(ledPin, HIGH);
    LED_state = HIGH;
  } else {
    digitalWrite(ledPin, LOW);
    LED_state = LOW;
  }
}
