nt sensVal;
int sensorPin = 0; // Propella
int ledPin = 11;
int light[4] = {0, 64, 128, 255}; 
int i=0;
int led_state;

void setup() {
  Serial.begin(9600); 
  pinMode(ledPin, OUTPUT);
  led_state = HIGH;
}

void loop() {
  sensVal = analogRead(sensorPin); // read sensorPin.

  if ( sensVal >= 400 ) {
    sensVal = 300; 
  }
  analogWrite(ledPin, light[sensVal/100]);
  Serial.println(sensVal/100);
  Serial.println(light[sensVal/100]); // Display sensVal to Sirial Monitor

  sec_delay(1); 
}

void sec_delay(int sec) {
  delay(sec*1000);
}
