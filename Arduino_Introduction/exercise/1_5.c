int ledRed = 10;
int ledOrange = 11;
int ledGreen = 12;
int SWITCH = 13;

int Red_state;
int Orange_state;
int Green_state;
int now_state;

int Red_length = 1;
int Orange_length = 2;
int Green_length = 3;

int SW_now;
int SW_last;

void setup() {
  pinMode(SWITCH, INPUT);
  pinMode(ledRed, OUTPUT);
  pinMode(ledOrange, OUTPUT);
  pinMode(ledGreen, OUTPUT);
  Red_state = 0;
  Orange_state = 1;
  Green_state = 2;
  now_state = 0;

}

long lastDebounceTime = 0;
long debounceDelay = 50;
int LED_state;


void loop() {
  SW_now = digitalRead(SWITCH);
  if (SW_last == LOW && SW_now == HIGH) {
    int now = millis(); // set now time.
    if ((now - lastDebounceTime) > debounceDelay) {
      onPress(); // change LED state.
      now_state++;
    }
    lastDebounceTime = now;
  }
  SW_last = SW_now;
}

void onPress() {
  if (Red_state == now_state%3) {
    digitalWrite(ledGreen, LOW); 
    digitalWrite(ledRed, HIGH);
  }   
  if (Orange_state == now_state%3) {
    digitalWrite(ledRed, LOW);    
    digitalWrite(ledOrange, HIGH);
  }  
  if (Green_state == now_state%3) {
    digitalWrite(ledOrange, LOW); 
    digitalWrite(ledGreen, HIGH);
  } 
}
