int Do = 956;
int Re = 851;
int Mi = 758;
int Fa = 716;
int So = 638;
int Ra = 568;
int Si = 506;
int DoU =478;

int B16 =125;
int B8 = B16*2;
int B4 = B16*4;
int B2 = B16*8;
int i;

int SWITCHS[9];
int TONES[9] = {Do, Re, Mi, Fa, So, Ra, Si, DoU};
int SW_now;
int SW_last;

int speaker = 2;

void sound (int tone_delay) {
  digitalWrite(speaker, HIGH);
  delayMicroseconds(tone_delay);
  digitalWrite(speaker, LOW); 
  delayMicroseconds(tone_delay);
}

void play (int tone, int length) {
  unsigned long tone_end_time;
  tone_end_time = millis() + length;

  while (millis() < tone_end_time) {
    sound(tone);
  } 
}

void rent (int length) {
  delay(length); 
}

void setup() {
  SW_now = LOW;
  Serial.begin(9600);
  pinMode(speaker, OUTPUT); 
  for(i = 5; i < 13; i++) {
    SWITCHS[i-5] = i;
    pinMode(i, INPUT);
    digitalWrite(i, HIGH); 
  }
}

void loop() {
  for(i = 0; i < 9; i++) {
    Serial.println(i);
    SW_now = digitalRead(SWITCHS[i]);
    while (SW_now == LOW) {
      play(TONES[i], B8);
      Serial.println(i);
      SW_now = digitalRead(SWITCHS[i]);
    }
    SW_now == HIGH;
  }
}

