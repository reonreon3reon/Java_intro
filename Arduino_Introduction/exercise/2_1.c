int ledRed = 11;

int light[4] = {0, 64, 128, 255}; 
int i=0;

int Red_state;

void setup() {
  pinMode(ledRed, OUTPUT);
  Red_state = HIGH;
}

void loop() {
  analogWrite(ledRed, light[i%4]);
  if (Red_state == HIGH) {
    digitalWrite(ledRed, HIGH);
    Red_state = LOW;
  }
  i++;
  sec_delay(1);
}

void sec_delay(int sec) {
  delay(sec*1000);
}
