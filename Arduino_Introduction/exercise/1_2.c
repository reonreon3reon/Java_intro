int SWITCH = 13;
int ledPin = 12;
int SW_now, SW_last; // $B8=:_$H2a5n$N%9%$%C%A$N>uBV(B

int LED_state;

void setup() {
  pinMode(SWITCH, INPUT);
  pinMode(ledPin, OUTPUT);
  LED_state = LOW; // LED $B$r>CEt$K=i4|2=$9$k(B
}

void loop() {
  Serial.println(LED_state);
  SW_now = digitalRead(SWITCH); // $B%9%$%C%A$N>uBV$rFI$`(B
  if (SW_last == LOW && SW_now == HIGH) { // $BA0$,(BHIGH, $B:#$,(BHIGH$B$N>l9g(B
    onPress(); // LED$B$N>uBV$rJQ$($k(B
  }
  SW_last = SW_now; // $B:#$N>uBV$r2a5n$N>uBV$K$9$k(B
}

// $B%9%$%C%A$,2!$5$l$?;~$K(B, LED$B$N>uBV$rJQ$($k4X?t(B
void onPress() {
  if (LED_state == LOW) {
    digitalWrite(ledPin, HIGH); // LED$B$r>CEt$+$iE@Et(B
    LED_state = HIGH;
  } else {
    digitalWrite(ledPin, LOW); // LED$B$rE@Et$+$i>CEt(B
    LED_state = LOW;
  }
}
