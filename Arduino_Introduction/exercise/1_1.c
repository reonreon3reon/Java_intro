int SWITCH = 13; // $B%9%$%C%A$NF~NO$r%G%8%?%k%T%s(B13$B$K@\B3(B
int ledPin = 12; // $B%G%8%?%k%T%s(B12$B$K(BLED$B$r@\B3(B

void setup() {
  pinMode(SWITCH, INPUT); // SWITCH(13$BHV(B)$B%T%s$r(B, $BF~NO%b!<%I$K(B
  pinMode(ledPin, OUTPUT); // ledPin(12$BHV%T%s(B)$B$r(B, $B=PNO$K;HMQ(B
}

void loop() {
  if (digitalRead(SWITCH) == HIGH) // $B%?%/%H%9%$%C%A$,2!$5$l$k$+3NG'(B
    digitalWrite(ledPin, HIGH); //$B!!(BLED$B$rE@Et(B
  else
    digitalWrite(ledPin, LOW); // LED$B$r>CEt(B
}
