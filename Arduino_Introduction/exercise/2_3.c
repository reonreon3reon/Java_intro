nt sensVal;
int sensorPin = 0; // Propella
int ledPin1 = 11;
int ledPin2 = 10;
int ledPin3 = 9;
int ledPin4 = 8;

int i=0;

void setup() {
  Serial.begin(9600); 
  pinMode(ledPin1, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  pinMode(ledPin3, OUTPUT);
  pinMode(ledPin4, OUTPUT);
}

void loop() {
  sensVal = analogRead(sensorPin); // read sensorPin.
  if(sensVal > 0) {
    digitalWrite(ledPin1, HIGH);
    if (sensVal > 100) {
      digitalWrite(ledPin2, HIGH); 
      if (sensVal > 200) {
        digitalWrite(ledPin3, HIGH);
        if (sensVal > 300) 
          digitalWrite(ledPin4, HIGH);
      }
    } 
  } else {
    digitalWrite(ledPin1, LOW);
    digitalWrite(ledPin2, LOW);
    digitalWrite(ledPin3, LOW);
    digitalWrite(ledPin4, LOW);
  }

  Serial.println(sensVal/100);

  sec_delay(1.5); 
}

void sec_delay(int sec) {
  delay(sec*1000);
}
