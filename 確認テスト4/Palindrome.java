//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-07-28 

import java.io.*;

public class Palindrome {
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      String line = reader.readLine();
      int i = 0;
      int len = line.length();
      while (i + i < len && line.charAt(i) == line.charAt(len - i -1)) {
        i++;
      }
      System.out.print(line + "は回文で");
      if ( i + i >= len) {
        System.out.println("す．");
      } else {
        System.out.println("はありません．");
      }
    } catch (IOException e) {
      System.out.println(e);
    }
  }
}
