//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-07-28 

import java.io.*;

public class ConvertA_J {
  public static void main(String[] args) {
    int i = 0 ;
    while (i < 20) {
      System.out.println(i);
      i = i + 2;
    }
  }
}
