//  -*- coding: utf-8 -*- 
//  vim: set fileencoding=utf-8 : 
//  StudentNumber: s1511392 
//  Author:   Reon Nishimura  
//  Created:  2015-07-28 

import java.io.*;

public class Palindrome2 {
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    try {
      String line = reader.readLine();
      int i = 0;
      int len = line.length();
      boolean ok_sofar = true;
      while (i < len/2 && ok_sofar) {
        if (line.charAt(i) != line.charAt(len -i - 1)) {
          ok_sofar = false;
        }
        i++;
      }
      System.out.print(line + "は回文で");
      if (ok_sofar) {
        System.out.println("す．");
      } else {
        System.out.println("はありません．");
      }
    } catch (IOException e) {
      System.out.println(e);
    }
  }

}
